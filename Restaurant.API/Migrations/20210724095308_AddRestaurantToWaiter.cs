﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Restaurant.API.Migrations
{
    public partial class AddRestaurantToWaiter : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<Guid>(
                name: "RestaurantId",
                table: "Waiters",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Waiters_RestaurantId",
                table: "Waiters",
                column: "RestaurantId");

            migrationBuilder.AddForeignKey(
                name: "FK_Waiters_Restaurants_RestaurantId",
                table: "Waiters",
                column: "RestaurantId",
                principalTable: "Restaurants",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Waiters_Restaurants_RestaurantId",
                table: "Waiters");

            migrationBuilder.DropIndex(
                name: "IX_Waiters_RestaurantId",
                table: "Waiters");

            migrationBuilder.DropColumn(
                name: "RestaurantId",
                table: "Waiters");
        }
    }
}
