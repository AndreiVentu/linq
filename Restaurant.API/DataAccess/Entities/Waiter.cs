﻿using System.Collections.Generic;

namespace Restaurant.Domain
{
	public class Waiter : BaseEntity
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

        public int Salary { get; set; }
        public int Age { get; set; }

		public int Phone { get; set; }

		public RestaurantBranch Restaurant { get; set; }

		public ICollection<OrderTable> Tables { get; set; }

		public Waiter()
		{
			FirstName = "Cutare";
			LastName = "Cutarescu";
			Age = 20;
			Salary = 2000;
			Phone = 072222;
			Restaurant = null;
			Tables = new List<OrderTable>();
		}
	}
}
