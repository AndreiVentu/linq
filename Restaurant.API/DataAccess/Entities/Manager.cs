﻿using System.ComponentModel.DataAnnotations;

namespace Restaurant.Domain
{
	public class Manager : BaseEntity
	{
		[Required]
        [MaxLength(20)]
		public string Name { get; set; }

		public string Email { get; set; }

		public virtual RestaurantBranch Restaurant { get; set; }

	}
}
