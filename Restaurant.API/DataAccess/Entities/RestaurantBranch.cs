﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Restaurant.Domain
{
	public class RestaurantBranch : BaseEntity
	{
		public string Name { get; set; }

		public string Adress { get; set; }

		public string Description { get; set; }
        public string Email { get; set; }
		public int Phone { get; set; }


		public string City { get; set; }

		public ICollection<OrderTable> Tables { get; set; }

		public RestaurantBranch(string name, string adress, string description, int phone, string email, string city)
		{
			Name = name;
			Adress = adress;
			Description = description;
			Phone = phone;
			Email = email;
			City = city;
		}
	}
}
