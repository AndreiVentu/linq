﻿using System.Linq;
using Restaurant.Domain;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Restaurant.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ManagersController : ControllerBase
    {
	    private readonly DatabaseContext _databaseContext;
        public ManagersController(DatabaseContext databaseContext)
        {
            // TODO use DI and inject your DB context in constructor
            _databaseContext = databaseContext;
        }

        [HttpGet("customFilter")]
        public IEnumerable<Manager> GetManagersFiltered()
        {
            // Return all managers with email domain -> 'demo.com' and FirstName -> 'Alex'
            var a = _databaseContext.Managers.Where(c => c.Name=="Alex" && c.Email.Contains("@demo.com")).ToList();
            return a;
        }
    }
}