﻿using Restaurant.Domain;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;

namespace Restaurant.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class RestaurantBranchesController : ControllerBase
    {
	    private readonly DatabaseContext _databaseContext;
        public RestaurantBranchesController(DatabaseContext databaseContext)
        {
            // TODO use DI and inject your DB context in constructor
            _databaseContext = databaseContext;
        }

        [HttpPost]
        public void Add(string name, string adress, string description, int phone, string email, string city)
        {
            // TODO Add restaurant branch in DB
            // Create new RestaurantBranch inside method OR add RestaurantBranch as parameter and call endpoint POST restaurantBranches
            var restaurant = new RestaurantBranch(name, adress, description, phone, email, city);
            _databaseContext.Restaurants.Add(restaurant);
            _databaseContext.SaveChanges();
        }
        
        [HttpDelete("id")]
        public void Add(Guid id)
        {
            // Remove restaurant branch by id
            var delrestaurant = _databaseContext.Restaurants.Find(id);
            _databaseContext.Restaurants.Remove(delrestaurant);
            _databaseContext.SaveChanges();
        }
        
        [HttpGet]
        public IEnumerable<RestaurantBranch> GetAll()
        {
            // Return all restaurant branches with all necessary data manager, waiters, orderTables etc
            // P.S. Change return type
            return _databaseContext.Restaurants;
        }
    }
}