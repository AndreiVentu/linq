﻿using System.Collections.Generic;
using System.Linq;
using Restaurant.Domain;
using Microsoft.AspNetCore.Mvc;

namespace Restaurant.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class OrderTablesController : ControllerBase
    {
	    private readonly DatabaseContext _databaseContext;
        public OrderTablesController(DatabaseContext databaseContext)
        {
            // TODO use DI and inject your DB context in constructor
            _databaseContext = databaseContext;
        }

        [HttpGet]
        public IEnumerable<OrderTable> GetAllOrderTables()
        {
            // TODO Return all order tables with all waiters 
            return _databaseContext.OrderTables;

        }

        [HttpGet("customFilter")]
        public IEnumerable<OrderTable> GetAllOrderTablesFiltered()
        {
            // TODO Return all orderTables filtered by:
            // IsVip true
            // Have more than 2 waiters
            // P.S. Change return type
            return _databaseContext.OrderTables.Where(t => t.isVip).ToList();
        }
    }
}