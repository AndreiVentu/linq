﻿using System.Collections.Generic;
using System.Linq;
using Restaurant.Domain;
using System;
using Microsoft.AspNetCore.Mvc;

namespace Restaurant.API.Controllers
{
    
    [ApiController]
    [Route("[controller]")]
    public class WaitersController : ControllerBase
    {
	    private readonly DatabaseContext _databaseContext;
        public WaitersController(DatabaseContext databaseContext)
        {
           // TODO use DI and inject your DB context in constructor
           _databaseContext = databaseContext;
        }

        [HttpGet("customFilter")]
        public IEnumerable<Waiter> GetWaitersFiltered()
        {
            // TODO Filter waiters by:
            // Age between 18 and 25
            // Salary greater than 2544.55
            // Have more than 2 order tables
            // P.S. Change return type
            return _databaseContext.Waiters.Where(w => w.Age>=18 && w.Age <= 25 && w.Salary>2544.55 && w.Tables.Count>2);
        }

        [HttpGet("customFilter2")]
        public IEnumerable<Waiter> GetWaitersFiltered2()
        {
            // TODO Return all waiters filtered using custom method from WaiterValidator
            // P.S Change return type

            return WaiterValidator.Validate(_databaseContext);
        }

        [HttpGet("maxSalary")]
        public int GetMaxSalary()
        {
            // TODO Return the highest salary from waiters
            // Example: Waiter1 : 1500, Waiter2: 900, Waiter3: 2400
            // Method should return 2400
            int a = _databaseContext.Waiters.Max(w => w.Salary);
            return a;
        }

        [HttpGet("sumOfAllSalaries")]
        public int GetSumOfAllSalaries()
        {
            // TODO Return the sum of all salaries from all waiters (it doesn't matter if it belongs to a specific restaurant)
            // Example: Waiter1 : 1500, Waiter2: 900, Waiter3: 2400
            // Method should return 4800
            int a = _databaseContext.Waiters.Sum(w => w.Salary);
            return a;
        }

        [HttpGet("sumOfAllSalariesBasedOnRestaurantBranchId/{restaurantBranchId}")]
        public int GetSumOfAllSalariesBasedOnRestaurantBranchId(Guid restaurantBranchId)
        {
            // TODO Return the sum of all salaries from all waiters belonging to a restaurant (identify restaurant by id)
            var a = _databaseContext.Waiters.Where(w => w.Restaurant.Id == restaurantBranchId);
            int b = a.Sum(w => w.Salary);
            return b;
        }

        [HttpPost("orderTableId")]
        public void AddWaiterToATable(Guid orderTableId)
        {
            // TODO Add waiter to existing Order Table
            // Create new Waiter inside method var waiter = new Waiter() .... OR add Waiter as parameter and call endpoint POST waiters/{orderTableId}
            var waiter = new Waiter();
            var table = _databaseContext.OrderTables.Where(t => t.Id == orderTableId);
            waiter.Tables.Add((OrderTable)table);
        }
    }

    public class WaiterValidator
    {
        // Validator should return true if received waiter as parameter have:
        // Age between 25 and 50
        // Salary greater than 1500
        public static IEnumerable<Waiter> Validate(DatabaseContext databaseContext)
        {
            // TODO update logic
            return databaseContext.Waiters.Where(w => w.Age >= 25 && w.Age <= 50 && w.Salary > 1500);
        }
    }
}